from django.conf.urls.defaults import *

urlpatterns = patterns('contest.views',
    # contest API
    url(r'^user/(?P<user_id>\d+)/$', 'showUserDesigns'),
    # url(r'^leaderboard/$', 'showLeaderboard'),
    url(r'^create/$', 'createDesign'),
    url(r'^(?P<code>[\w\._-]+)/$', 'showDesign'),
)