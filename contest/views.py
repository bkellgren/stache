import sys

from django.contrib import messages

from django.conf import settings
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext, Context
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect

from django.core.mail import send_mail

from contest.forms import CreateDesignForm

from contest.models import Design, Face


def showDesign(request, code, template_name='design.html'):

    fullsrc = False

    if request.GET.get('fullsrc'):
        fullsrc = True

    design = get_object_or_404(Design, code=code)

    return render_to_response(template_name, {
        'fullsrc': fullsrc,
        'designs': [design]
    }, context_instance=RequestContext(request))

def showLeaderboard(request):
    return "Leaderboard!!!!"

def showUserDesigns(request, user_id=False, template_name='designs-user.html'):
    fullsrc = False

    if request.GET.get('fullsrc'):
        fullsrc = True

    if user_id:
        if request.user.is_authenticated():
            designs = Design.objects.filter(user=request.user.id)
        else:
            messages.add_message(request, messages.ERROR, 'Authentication required for this page.')
            designs = []
            return HttpResponseRedirect('/')
    else:
        designs = []

    return render_to_response(template_name, {
        'fullsrc': fullsrc,
        'designs': designs
    }, context_instance=RequestContext(request))


def createDesign(request):

    if request.POST:
        if request.user.is_authenticated():

            form = CreateDesignForm(request.POST, request.FILES)

            if form.is_valid():

                new_design = form.save(commit=False)

                if new_design:

                    new_design.user = request.user
                    new_design.face = Face.objects.get(id=request.POST.get('face_id'))

                    new_design.save()

                    send_mail('New Design Submission', 'Thanks for submitting a Chicago Stache design "' + new_design.title + '"! Just a reminder that you need to be the first to donate to your new design in order for it to display publically for others to contribute as well. Make sure to copy the design code (' + new_design.code + ') when you donate at the following location... ' + new_design.face.url, 'info@chicagostache.com', [request.user.email], fail_silently=True)

                    return HttpResponse(new_design.code)

                else:

                    return HttpResponse('Something went wrong')
            else:
                return HttpResponse('this post aint valid data yo')

        else:
            return HttpResponse('you gots to be authenticated yo')
    else:
        messages.add_message(request, messages.ERROR, 'That page is not for humans...')
        return HttpResponseRedirect('/')
