from django import forms
from django.forms import ModelForm
from contest.models import Design

class CreateDesignForm(ModelForm):
    face_id = forms.CharField(widget=forms.HiddenInput())
    front_string = forms.CharField(widget=forms.HiddenInput())
    side_string = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Design
        fields = ('title', 'front_string', 'side_string')