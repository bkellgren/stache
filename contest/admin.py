from contest.models import Face, Design, Vote
from django.contrib import admin

admin.site.register(Face)
admin.site.register(Design)
admin.site.register(Vote)