import re

import sys

from django.conf import settings

from base64 import decodestring

from datetime import *

from django.db import models

from django.contrib.auth.models import User

from django.utils.translation import ugettext as _


class Face(models.Model):
    name = models.CharField(max_length=255)
    handle = models.CharField(max_length=255)
    front = models.ImageField(upload_to='faces/front/', blank=True, null=True,)
    side = models.ImageField(upload_to='faces/side/', blank=True, null=True,)
    thumb = models.ImageField(upload_to='faces/thumbs/', blank=True, null=True,)
    bw_thumb = models.ImageField(upload_to='faces/thumbs/bw/', blank=True, null=True,)
    bio = models.TextField(_('personal statement'), blank=True, null=True,)
    url = models.CharField(max_length=500)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.handle


class Design(models.Model):
    user = models.ForeignKey(User, verbose_name=_('user'))
    face = models.ForeignKey(Face, verbose_name=_('face'))
    title = models.CharField(max_length=255)
    front = models.ImageField(upload_to='designs/front/', blank=True, null=True,)
    front_string = models.TextField(blank=True, null=True,)
    side = models.ImageField(upload_to='designs/side/', blank=True, null=True,)
    side_string = models.TextField(blank=True, null=True,)
    created_on = models.DateTimeField(auto_now_add=True)
    code = models.CharField(max_length=255, blank=True, null=True,)
    active = models.BooleanField(default=False)

    #def get_donation_total():

    def save(self, *args, **kwargs):

        if not self.code:

            # create unique code for voting

            random_code = User.objects.make_random_password(length=6, allowed_chars='ABCDEFGHJKLMNPQRSTUVWXYZ23456789')

            while Design.objects.filter(code=random_code):
                random_code = User.objects.make_random_password(length=6, allowed_chars='ABCDEFGHJKLMNPQRSTUVWXYZ23456789')

            self.code = random_code

            #create and save front img

            file_name = str(self.face)

            if self.front_string:
                front_name = 'designs/front/' + file_name + '_' + self.code + '.png'
                with open(settings.MEDIA_ROOT + front_name,"wb") as front_output:
                    front_output.write(decodestring(re.search(r'base64,(.*)', self.front_string).group(1)))

                self.front = '/media/' + front_name


            #create and save side img

            if self.side_string:
                side_name = 'designs/side/' + file_name + '_' + self.code + '.png'
                with open(settings.MEDIA_ROOT + side_name,'wb') as side_output:
                    side_output.write(decodestring(re.search(r'base64,(.*)', self.side_string).group(1)))

                self.side = '/media/' + side_name

            self.front_string = ''
            self.side_string = ''

        super(Design, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.face.name + ' ' + self.title + ' ' + self.code


class Vote(models.Model):
    design = models.ForeignKey(Design, verbose_name=_('design'))
    name = models.CharField(max_length=255, blank=True, null=True)
    amount = models.CharField(max_length=255)
    message = models.CharField(max_length=255, blank=True, null=True)

    def save(self, *args, **kwargs):

        design = Design.objects.get(id=self.design.id)
        design.active = True
        design.save()

        super(Vote, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.design.title
