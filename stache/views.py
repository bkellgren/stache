from django.conf import settings
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext, Context
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect

from django.contrib import messages

from contest.models import Face, Design
from contest.forms import CreateDesignForm

from django.core.cache import cache


def home(request, template_name="home.html"):

    fullsrc = False

    if request.GET.get('fullsrc'):
        fullsrc = True

    design_form = CreateDesignForm(initial={
        'title': '<Title Here>',
        'user': request.user.id
    })

    if cache.get('home_faces'):
        faces = cache.get('home_faces')
    else:
        faces = Face.objects.filter(active=True)
        cache.set('home_faces', faces, 300)

    if cache.get('home_designs'):
        designs = cache.get('home_designs')
    else:
        designs = Design.objects.filter(active=True).distinct('face')
        cache.set('home_designs', designs, 300)

    # messages.add_message(request, messages.SUCCESS, 'Hello world.')

    return render_to_response(template_name, {
        'fullsrc': fullsrc,
        'design_form': design_form,
        'faces': faces,
        'designs': designs
    }, context_instance=RequestContext(request))

def home_placeholder(request, template_name="home_placeholder.html"):

    fullsrc = False

    if request.GET.get('fullsrc'):
        fullsrc = True

    return render_to_response(template_name, {
        'fullsrc': fullsrc,
    }, context_instance=RequestContext(request))
