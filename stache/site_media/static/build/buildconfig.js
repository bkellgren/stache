{
    baseUrl: "../js",
    name: "main",
    out: "../js/script-built.js",
    paths: {
        'jquery': 'vendor/jquery-1.8.2.min',
        'dcupl': 'vendor/dcupl',
        'dcupl.hook': 'vendor/dcupl.hook',
        'carousel': 'vendor/carousel'
        'design': 'modules/design'
    }
}
