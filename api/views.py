from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from contest.models import Face, Design, Vote

from django.core import serializers
from django.forms.models import modelformset_factory

from django.views.decorators.cache import cache_page

def json_view(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        if not result:
            return HttpResponse('give me something to work with...')
        return HttpResponse(serializers.serialize('json', result), mimetype="application/json")
    return wrapper

@cache_page(60 * 5)
@json_view
def get_designs(request, design_id = None):
    if design_id:
        design = get_object_or_404(Design, id=design_id)
        if design.approve:
            return [design]
        else:
            return HttpResponse('this design has not yet been approved')
    else:
        return Design.objects.filter(active=True)

@cache_page(60 * 5)
@json_view
def get_designs_by_face(request, face_id):
    return Design.objects.filter(face_id=face_id, active=True)


@cache_page(60 * 5)
@json_view
def get_faces(request, face_id = None):
    if face_id:
        return get_object_or_404(Face, pk=face_id)
    else:
        return Face.objects.all()

@json_view
def get_votes(request, design_id):
    return Vote.objects.filter(design_id=design_id)
