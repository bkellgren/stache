from django.conf.urls.defaults import *

urlpatterns = patterns('api.views',
    # contest API
    url(r'^designs/$', 'get_designs'),
    url(r'^designs/(?P<design_id>\d+)/$', 'get_designs'),
    url(r'^designs/face/(?P<face_id>\d+)/$', 'get_designs_by_face'),
    url(r'^faces/$', 'get_faces'),
    url(r'^faces/(?P<face_id>\d+)/$', 'get_faces'),
    url(r'^votes/(?P<design_id>\d+)/$', 'get_votes'),
)
